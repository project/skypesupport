Introduction
=============
This module gives the users on your site the ability to offer live support via Skype.

Requirements
=============
This modules requires you to have a Skype Client installed. It is recommended that you have the latest version
for your platform (Windows, OS X, Linux). You also must have the setting "Allow My Status To Be Shown On The
Web" enabled which is located under the Privacy Preferences in Skype.

Installation
=============
	1) Unzip the skypesupport and place the entire skypesupport folder into your modules directory.
	2) Go to administer -> modules and enable the module. 

Configuration
==============
	1) Set up the permissions in administer -> access control.
	2) Then in access control, you can also enable the 'provide skype support' permission to the role of your choice
		which will enable the feature for the users that have that role or go to administer -> skypesupport and enable 
		some users to be 'support users'. 
	3)	Once these users are labeled support users, they now will have fields on their user edit page for a skype user 
		name, and statuses they can be in on Skype to accept support requests. 

		**NOTE**
		These fields must be filled in for the user to show up in the 'Available Users' list.

Contact information
====================
Please contribute your suggestions, bugs and issues at the project's page http://drupal.org/project/skypesupport

steve [at] stevemckenzie.ca
fusion94 [at] damagestudios.net